public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // TODO Complete me!
	WildCat cat;
	TrainCar next;

    public TrainCar(WildCat cat) {
		this.cat = cat;
        // TODO Complete me!
    }

    public TrainCar(WildCat cat, TrainCar next) {
		this.cat = cat;
		this.next = next; //menyimpan data sebelumnya(kereta)
        // TODO Complete me!
    }

    public double computeTotalWeight() {
		if (next == null){
			double TotalWeight = EMPTY_WEIGHT + this.cat.weight;
			return TotalWeight;
		}else{
			double TotalWeight = this.cat.weight + EMPTY_WEIGHT + next.computeTotalWeight();
			return TotalWeight;
		}
			
        // TODO Complete me!
      
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
		if(next == null){
			double TotalMassIndex = cat.computeMassIndex();
			return TotalMassIndex;
		}else{
			double TotalMassIndex = cat.computeMassIndex() + next.computeTotalMassIndex();
			return TotalMassIndex;
		}
    }

    public void printCar() {
        // TODO Complete me!
		if(next == null){
			System.out.print('('+ this.cat.name + ')');
		}else{
			String strRes = '(' + cat.name + ')'+ "--";
			System.out.print(strRes);
			next.printCar();
		}
    }
}
